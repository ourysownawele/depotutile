# Angular4 authentication   
- this is the front-end layer of the app using anuglar 4 , 
- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

## Idea of the app 
 - simple user registration /  authentication 

## Development server

### installing the node module :
Use the following command in the project folder `npm install --build-from-source`  [NPM] https://www.npmjs.com/

### running the server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
# README #

This README would normally document whatever steps are necessary to get your application up and running.


### Git global setup ###

```
git config --global user.name "SOW Mamadou Oury"
git config --global user.email "mamadououry.sow@scalian.com"
```
Create a new repository

```
git clone ssh://git@archiforge:2222/scalab/scalab-profil-service.git
git clone http://archiforge/scalab/scalab-profil-service.git
cd scalab-profil-service
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

Existing folder or Git repository NB: existing_folder est le repertoire ou vous avez votre projet: exemple existing_folder/MonProjet/src
```
cd existing_folder
git init
git add .
git commit -m "commit initial"
git remote add origin https://ourysow@bitbucket.org/ourysow/jsf_spring_jpa.git
git push -u origin --all   //ou git push -u origin master
git push -u origin --tags

```
### importer un projet git existant vers un nouveau depor git

git clone --mirror : pour cloner toutes les références (s'engage, tags, branches)
git push --mirror : pour pousser tout
qui donnerait:
```
git clone --mirror git@bitbucket.org:projet-pro/backend_old.git
```
 Make a bare mirrored clone of the repository
```
cd backend_old
git remote set-url --push origin git@bitbucket.org:projet-pro/backend_new.git
```
 Set the push location to your mirror
```
git push --mirror
```

### Apprendre à utiliser Git: - Les branches ###
0- On initialise le projet si c'est pas fait
```
$ cd existing_folder  -> aller dans le projet
$ git init            -> initialiser le projet comme un projet git
$ git add -A          ->  ajouter tout le contenu du projet (-A veut dire all)
$ git commit -m  "message du commit"
$ git branch    -> montre l'ensemble des nos branch 
```
`
Vérifier la branche courance:  une * sera devant la branch courante
```
$ git branch  
```
1- On crée la branch d'abord puis se rendre dans la branche crée
```
$ git branch NomDeLaBranch   // on crée
$ git checkout NomDeLaBranch  //puis on se rend dans la branche
```
2- Ou bien On crée la branch et en même temp on se rend dans la branch
```
$ git checkout -b NomDeLaBranch  
```
Pour vérifier on fait git branch qui nous montre la branche courante avec une *
```
$ git branch   
```
3- Travailler sur la nouvelle fonctionnalité,si vous etes satisfait:
```
$ git add -A          
$ git commit -m  "ajout d'une fonctionnalité dans la branch NomDeLaBranch"
```
4- Rétour sur la brnch master ici sans l'option -b car vous ne voulez pas créer
```
$ git checkout master
$ git branch     -> vous confirmera de votre retour          
```
NB: au moment de notre retour git nous met à l'état ou on avait quitter la branche c'est à dire il ne prend pas en compte les modifications de la nouvelle fonctionnalité  de la branche qu'on a crée. d'ou la notion de fusion des branches ou merge
5 - Faison le merge(fusion) entre la branche master et celle qu'on vient de quitter on la nommé NomDeLaBranch. git status nous confirmera que la nouvelle fonctionnalité a été bien fusionnée avec la branch master

```
$ git merge NomDeLaBranch
$ git status     -> vous confirmera de votre retour          
```
5-a Si vous avez effectué des modifications dans les 2 branches au même endroit et que vous essayez de faire le merge, git merge vous donnera un message d'erreur car il ne s'aura plus quelle version il va prendre. Donc il faudra d'abord faire vos choix en résolvant les conflits

```
$ git diff /cheminDuFichier/LeFichier   //regarder la difféence, faite vos choix      
```
5-b Recommencer le merge
```
$ git merge NomDeLaBranch         
```
6- Après avoir reussi la fusion on peut supprimer la branche qui nous a servi à créer la fonctionnalité avec l'option -b
NB: l'option -b suprime la branche fusionnée. Si on veut supprimer la branche même si elle n'a pas été fusionner on on met l'option -D ce qui donnera git branch -D NomDeLaBranch
```
$ git merge NomDeLaBranch            
```
7- "Problème courant": si on se rend dans une branche ou on travail sur une nouvelle fonctionnalité. on fait des modifications puis on sort de cette branch pour revenir dans la branche master sans faire (git add -A et git commit -m de l'etape 3). on transportera les modif de cette dernière dans la branch master. à vérifier par:
```
$ git status          
```  
Donc vous devez rétouner vers la branche NomDeLaBranch en faisant       
```
$ git checkout NomDeLaBranch  // se rend dans la branche
$ git add -A          
$ git commit -m  "ajout d'une fonctionnalité dans la branch NomDeLaBranch"
$ git checkout master    // et en fin retourner
```
### Suppression d'un fichier ou rep du git distant ### 
```
$ cd DossierContenantLesRessourcesASupp
$ git rm Nom_DU_Fichier
$ git commit -m  "Supression du fichier Nom_DU_Fichier"
$ git push origin master
```
### Suppression d'un repertoire du git distant ### 
```
$ cd DossierContenantLesRessourcesASupp
$ git rm -r Nom_DU_REPERTOIRE
$ git rm -r -f Nom_DU_REPERTOIRE   Remarque:mettre l'option -f si vous souhaiter forcer 
$ git commit -m  "Supression du fichier Nom_DU_REPERTOIRE"
$ git push origin master
```
### Les Log ###
```
$ git log             // voir tous les log
$ git log --oneline       // affiche une seule qui décrit chacun des commit
$ git log --oneline --graph     // représente graphiquement en une ligne chacun des commit
```

### Class StringUtils ###

Class StringUtils: java.lang.Object extended by org.apache.commons.lang.StringUtils

```
<dependency>
    <groupId>commons-lang</groupId>
    <artifactId>commons-lang</artifactId>
    <version>2.6</version>
</dependency>
```

* **IsEmpty/IsBlank** - vérifie si une chaîne contient du texte
* **Trim/Strip** - supprime les espaces blancs avant et arrière
* **Equals** - compare deux chaînes nulles
* **startsWith** - vérifie si une chaîne commence par un préfixe nul
* **endsWith** -  vérifier si une chaîne se termine par un suffixe nu
* **IndexOf/LastIndexOf/Contains** - null-safe index-of checks
* ** IndexOfAny/LastIndexOfAny/IndexOfAnyBut/LastIndexOfAnyBut** - index-of any of a set of Strings
* **ContainsOnly/ContainsNone/ContainsAny** - does String contains only/none/any of these characters
* **Substring/Left/Right/Mid** - null-safe substring extractions
* **SubstringBefore/SubstringAfter/SubstringBetween** - extraction de sous-chaîne par rapport à d'autres chaînes
* **Remove/Delete** - removes part of a String
* **Replace/Overlay** - Searches a String and replaces one String with another
* **UpperCase/LowerCase/SwapCase/Capitalize/Uncapitalize** - changes the case of a String
* **CountMatches** - counts the number of occurrences of one String in another
* **IsAlpha/IsNumeric/IsWhitespace/IsAsciiPrintable** - checks the characters in a String
* **Reverse/ReverseDelimited** - reverses a String
* **Abbreviate** - abbreviates a string using ellipsis
* **Difference** - compares Strings and reports on their differences
//

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
